/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesentreobjetos;

import java.util.Date;

/**
 *
 * @author allanmual
 */
public class Movimiento {

    private Date fecha;
    private char tipo;
    private float importe;
    private double saldo;

    public Movimiento(Date fecha, char tipo, float importe, double  saldo) {
        this.fecha = fecha;
        this.tipo = tipo;
        this.importe = importe;
        this.saldo = saldo;
    }

}

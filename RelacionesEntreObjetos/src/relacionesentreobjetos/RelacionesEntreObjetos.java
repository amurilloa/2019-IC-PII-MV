/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesentreobjetos;


/**
 *
 * @author allanmual
 */
public class RelacionesEntreObjetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        Cliente cliente1 = new Cliente("Allan", "Murillo Alfaro");
        Cuenta cuenta1 = new Cuenta(123,5000,0.02,cliente1);
        
        cuenta1.setTitular(null);
        
        
        System.out.println(cuenta1.getNumero() + " - " + cuenta1.leerTitular().getNombre());

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesentreobjetos;

import java.util.Date;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Cuenta {

    private long numero;
    private double saldo;
    private double interesAnual;
    private Cliente titular;
    private LinkedList<Movimiento> movimientos;

    public Cuenta(long numero, double saldo, double interesAnual, Cliente titular) {
        this.numero = numero;
        this.saldo = saldo;
        this.interesAnual = interesAnual;
        this.titular = titular;
        movimientos = new LinkedList<>();
    }

    public void ingreso(int cantidad) {
        saldo += cantidad;
        movimientos.add(new Movimiento(new Date(), 'I', cantidad, saldo));
    }

    public void reintegro(int cantidad) {
        saldo -= cantidad;
        movimientos.add(new Movimiento(new Date(), 'R', cantidad, saldo));
    }

    public void ingresoInteresMes() {
    }

    public boolean enRojos() {
        return saldo < 0;
    }

    public int leerSaldo() {
        return (int) saldo;
    }

    public Cliente leerTitular() {
        return titular;
    }

    public void salvar() {
    }

    public long getNumero() {
        return numero;
    }

    public void setTitular(Cliente titular) {
        this.titular = titular;
    }

}

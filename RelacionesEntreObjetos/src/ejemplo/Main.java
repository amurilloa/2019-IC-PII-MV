/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {
        //Capitulo 3: Introducción a Objetos
        //Clase | Instancia/Objeto
        Corazon c1 = new Corazon();
        Corazon c2 = new Corazon();
        System.out.println(c1);
        System.out.println(c2);
       
        c1.setRitmo(123);
        System.out.println(c1.getRitmo());
        
        
        Motor m1 = new Motor();
        m1.setRpm(1000);
        Motor m2 = new Motor();
        m2.setRpm(3000);
        
        System.out.println(m1);
        System.out.println(m2);
        
        Coche co1 = new Coche(m1);
        Coche co2 = new Coche(m2);
        Persona p1 = new Persona("Allan");
        Persona p2 = new Persona("Pedro");
        
        p2.setCoche(co1);
        co1.setConductor(p1);
        System.out.println(p2);
        System.out.println(co1);
        System.out.println(co2);


    }
}

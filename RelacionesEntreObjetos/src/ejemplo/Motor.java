/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

/**
 *
 * @author allanmual
 */
public class Motor {
    private int rpm;
    private boolean activo;

    public void setRpm(int rpm) {
        this.rpm = rpm;
    }

    public boolean isActivo() {
        return activo;
    }

    public void activa(){
        activo = true;
    }
    
    public void desactiva(){
        activo = false;
        rpm = 0;
    }

    @Override
    public String toString() {
        return "Motor{" + "rpm=" + rpm + ", activo=" + activo + '}';
    }
    
    
}

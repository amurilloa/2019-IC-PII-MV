/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

/**
 *
 * @author allanmual
 */
public class Corazon {

    //Atributos
    private int ritmo;

    //Constructores
    public Corazon() {
        ritmo = -1;
    }

    //Otros métodos
    public boolean estaVivo(){
        return ritmo > 0;
    }
    
    
    //Getters & Setters
    public int getRitmo() {
        return ritmo;
    }

    public void setRitmo(int ritmo) {
        this.ritmo = ritmo;
    }

    //ToString
    @Override
    public String toString() {
        return "Corazon{" + "ritmo=" + ritmo + '}';
    }
    

}

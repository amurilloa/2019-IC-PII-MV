
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

/**
 *
 * @author allanmual
 */
public class Persona {

    private String nombre;
    private Corazon corazon;
    private Coche coche;

    public Persona(String nombre) {
        corazon = new Corazon();
        this.nombre = nombre;
    }

    
    public void viaja() {
    }

    public void emociona() {
    }

    public void tranquila() {
    }

    public void setCoche(Coche coche) {
        this.coche = coche;
    }

    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", corazon=" + corazon + ", coche=" + coche + '}';
    }

   
}

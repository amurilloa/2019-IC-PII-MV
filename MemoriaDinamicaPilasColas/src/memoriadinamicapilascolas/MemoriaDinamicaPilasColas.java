/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoriadinamicapilascolas;

/**
 *
 * @author allanmual
 */
public class MemoriaDinamicaPilasColas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Nodo de la lista --> Persona
        //Administrador de la lista --> Lista
        Persona p1 = new Persona("A", "B");
        Persona p2 = new Persona("A", "C");
        Persona p3 = new Persona("Pedro");
        Persona p4 = new Persona("Maria");

        Lista list = new Lista();
        list.agregar(p1);
        list.agregar(p2);
        list.agregar(p3);
        list.agregar(p4);
        list.agregar(new Persona("Luis"));

        System.out.println(list);
        list.agregarInicio(new Persona("Juancho"));
        System.out.println(list);
        list.agregar(new Persona("Hector"));
        System.out.println(list);
        list.agregarFinal(new Persona("Lucía"));
        System.out.println(list);
        System.out.println(list.buscar(new Persona("Allan", "Murillo")));

    }

}

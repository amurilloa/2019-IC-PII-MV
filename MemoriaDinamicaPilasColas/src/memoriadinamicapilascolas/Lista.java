/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoriadinamicapilascolas;

/**
 *
 * @author allanmual
 */
public class Lista {

    private Persona inicio;
    private int cant;

    public void agregar(Persona p) {
        if (estaVacia()) {
            inicio = p;
        } else {
            Persona actual = inicio;
            while (actual.getSig() != null) {
                actual = actual.getSig();
            }
            actual.setSig(p);
        }
    }
    //            a
    //p1->p2->p3->p4->

    public void agregarInicio(Persona p) {
        p.setSig(inicio);
        inicio = p;
    }

    public void agregarFinal(Persona p) {
        agregar(p);
    }

    public void eliminar(Persona p) {
    }

    public void eliminar(int pos) {
    }

    public boolean buscar(Persona p) {
        Persona actual = inicio;
        while (actual != null) {
            if (actual.equals(p)) {
                return true;
            }
            actual = actual.getSig();
        }
        return false;
    }

    public Persona obtener(int pos) {
        return null;
    }

    public int buscarPos(Persona p) {
        return -1;
    }

    public void vaciar() {
        inicio = null;

    }

    public boolean estaVacia() {
        return inicio == null;
    }

    public int getCant() {
        return cant;
    }

    @Override
    public String toString() {
        String res = "";
        Persona actual = inicio;
        while (actual != null) {
            res += actual.getNombre() + "-->";
            actual = actual.getSig();
        }
        return res;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplodos;

/**
 *
 * @author allanmual
 */
public class Perro {
    
    private String nombre;
    private String raza;
    private String fechaNac;
    private char genero;
    private boolean pedigree;

    public Perro() {
    }

    public Perro(String nombre, String raza, String fechaNac, char genero, boolean pedigree) {
        this.nombre = nombre;
        this.raza = raza;
        this.fechaNac = fechaNac;
        this.genero = genero;
        this.pedigree = pedigree;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public boolean isPedigree() {
        return pedigree;
    }

    public void setPedigree(boolean pedigree) {
        this.pedigree = pedigree;
    }

    @Override
    public String toString() {
        return "Perro{" + "nombre=" + nombre + ", raza=" + raza + ", fechaNac=" + fechaNac + ", genero=" + genero + ", pedigree=" + pedigree + '}';
    }
    
    
}

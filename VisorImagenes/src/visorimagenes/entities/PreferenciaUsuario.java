/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.entities;

import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class PreferenciaUsuario extends Preferencia {

    private int idUP;
    private int valor;

    public PreferenciaUsuario() {
    }

    public PreferenciaUsuario(int idUP, int valor, int id, String preferencia, boolean activo) {
        super(id, preferencia, activo);
        this.idUP = idUP;
        this.valor = valor;
    }

    public int getIdUP() {
        return idUP;
    }

    public void setIdUP(int idUP) {
        this.idUP = idUP;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return preferencia;
    }

}

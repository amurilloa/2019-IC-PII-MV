/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allanmual
 */
public class Conexion {

    private static final String DRIVER = "org.postgresql.Driver";
    private static final String TIPO = "jdbc:postgresql://";
    private static final String SERVER = "localhost";
    private static final String PUERTO = "5432";
    private static final String DB = "programacion_dos";
    private static final String USER = "postgres";
    private static final String PASS = "postgres";

    public static Connection conexion() throws Exception {
        Connection c = null;
        try {
            Class.forName(DRIVER);
            c = DriverManager.getConnection(TIPO + SERVER + ":" + PUERTO + "/"
                    + DB, USER, PASS);
            return c;
        } catch (Exception ex) {
            throw new Exception("Problemas con la conexión al servidor");
        }
    }

//    public static void main(String[] args) {
//        try {
//            //Revisar la base de datos, la contraseña y agregar la librería
//            System.out.println("conectando");
//            conexion();
//            System.out.println("conectando");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import visorimagenes.entities.PreferenciaUsuario;
import visorimagenes.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class UsuarioDAO {

    public Usuario actualizar(Usuario usuario) {
        System.out.println("Actualizar");
        //1. Controlar el error 
        //2. Obtener la conexion 
        try (Connection con = Conexion.conexion()) {
            //3. Definir la consulta o script
            String sql = " UPDATE usuarios SET usuario=?, correo=?,"
                    + " contrasena=?, activo=? "
                    + " WHERE id=?";
            //4. Crear PreparedStatement
            PreparedStatement ps = con.prepareStatement(sql);

            //5. Llenar los parámetros(?) del script
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getCorreo());
            ps.setString(3, usuario.getContrasena());
            ps.setBoolean(4, usuario.isActivo());
            ps.setInt(5, usuario.getId());

            //6.Ejecutar la consulta o script
            int rows = ps.executeUpdate();
            if (rows > 0) {
                return usuario;
            } else {
                throw new RuntimeException("Problemas al actualizar el usuario,"
                        + " favor intente nuevamente");
            }
        } catch (Exception e) {
            throw new RuntimeException("Problemas al actualizar el usuario,"
                    + " favor intente nuevamente");
        }
    }

    public Usuario insertar(Usuario usuario) {
        System.out.println("Insertar");
        //1. Controlar el error 
        //2. Obtener la conexion 
        try (Connection con = Conexion.conexion()) {
            //3. Definir la consulta o script
            String sql = " INSERT INTO usuarios(usuario, correo, contrasena) "
                    + " VALUES (?, ?, ?) RETURNING id ";
            //4. Crear PreparedStatement
            PreparedStatement ps = con.prepareStatement(sql);

            //5. Llenar los parámetros(?) del script
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getCorreo());
            ps.setString(3, usuario.getContrasena());

            //6.Ejecutar la consulta o script
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                usuario.setId(rs.getInt("id"));
            }
            return usuario;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Problemas al registrar el usuario,"
                    + " favor intente nuevamente");
        }
    }

    public boolean guardarPreferencia(PreferenciaUsuario pu, int idUsuario) {
        try (Connection con = Conexion.conexion()) {
            //3. Definir la consulta o script
            String sql = " INSERT INTO usuarios_preferencias(id_usuario,"
                    + " id_preferencia, valor) VALUES(?,?,?)";
            //4. Crear PreparedStatement
            PreparedStatement ps = con.prepareStatement(sql);

            //5. Llenar los parámetros(?) del script
            ps.setInt(1, idUsuario);
            ps.setInt(2, pu.getId());
            ps.setInt(3, pu.getValor());

            //6.Ejecutar la consulta o script
            int can = ps.executeUpdate();
            return can > 0;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Problemas al registrar la preferencia del usuario,"
                    + " favor intente nuevamente");
        }
    }

    public Usuario autenticar(Usuario u) {
        //1. Controlar el error 
        //2. Obtener la conexion 
        try (Connection con = Conexion.conexion()) {
            //3. Definir la consulta o script
            String sql = "SELECT id, usuario, correo, contrasena, activo "
                    + " FROM usuarios WHERE (usuario = ? or correo = ?) "
                    + " and contrasena = ? ";
            //4. Crear PreparedStatement
            PreparedStatement ps = con.prepareStatement(sql);
            //5. Llenar los parámetros(?) del script (opcional)
            ps.setString(1, u.getUsuario());
            ps.setString(2, u.getCorreo());
            ps.setString(3, u.getContrasena());
            //6.Ejecutar la consulta o script
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }
            return new Usuario();
        } catch (Exception e) {
            throw new RuntimeException("Problemas al cargar las preferencias de usuario,"
                    + " favor intente nuevamente");
        }
    }

    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setId(rs.getInt("id"));
        u.setUsuario(rs.getString("usuario"));
        u.setCorreo(rs.getString("correo"));
        u.setContrasena(rs.getString("contrasena"));
        u.setActivo(rs.getBoolean("activo"));
        return u;
   }

}

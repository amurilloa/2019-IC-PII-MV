/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import visorimagenes.entities.Opcion;
import visorimagenes.entities.Preferencia;

/**
 *
 * @author allanmual
 */
public class PreferenciaDAO {

    public LinkedList<Preferencia> cargar() {
        LinkedList<Preferencia> preferencias = new LinkedList<>();
        //1. Controlar el error 
        //2. Obtener la conexion 
        try (Connection con = Conexion.conexion()) {
            //3. Definir la consulta o script
            String sql = " SELECT id, preferencia, activo FROM preferencias WHERE activo = true";
            //4. Crear PreparedStatement
            PreparedStatement ps = con.prepareStatement(sql);

            //5. Llenar los parámetros(?) del script (opcional)
            //6.Ejecutar la consulta o script
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                preferencias.add(cargar(rs));
            }
        } catch (Exception e) {
            throw new RuntimeException("Problemas al cargar las preferencias de usuario,"
                    + " favor intente nuevamente");
        }
        return preferencias;
    }

    private Preferencia cargar(ResultSet rs) throws SQLException {
        Preferencia p = new Preferencia();
        p.setId(rs.getInt("id"));
        p.setPreferencia(rs.getString("preferencia"));
        p.setActivo(rs.getBoolean("activo"));
        p.setOpciones(cargarOpciones(p.getId()));
        return p;
    }

    private LinkedList<Opcion> cargarOpciones(int idPreferencia) {
        LinkedList<Opcion> opciones = new LinkedList<>();
        //1. Controlar el error 
        //2. Obtener la conexion 
        try (Connection con = Conexion.conexion()) {
            //3. Definir la consulta o script
            String sql = "SELECT id, opcion, valor, valor_min,valor_max FROM "
                    + " preferencia_opciones WHERE id_preferencia = ?";
            //4. Crear PreparedStatement
            PreparedStatement ps = con.prepareStatement(sql);

            //5. Llenar los parámetros(?) del script (opcional)
            ps.setInt(1, idPreferencia);
            //6.Ejecutar la consulta o script
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                opciones.add(cargarOpcion(rs));
            }
        } catch (Exception e) {
            throw new RuntimeException("Problemas al cargar las opciones de usuario,"
                    + " favor intente nuevamente");
        }
        return opciones;
    }

    private Opcion cargarOpcion(ResultSet rs) throws SQLException {
        Opcion op = new Opcion();
        op.setId(rs.getInt("id"));
        op.setOpcion(rs.getString("opcion"));
        op.setValor(rs.getInt("valor"));
        op.setValorMin(rs.getInt("valor_min"));
        op.setValorMax(rs.getInt("valor_max"));
        return op;
    }

}

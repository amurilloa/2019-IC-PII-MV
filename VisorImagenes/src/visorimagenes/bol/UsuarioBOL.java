/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.bol;

import org.apache.commons.codec.digest.DigestUtils;
import visorimagenes.dao.UsuarioDAO;
import visorimagenes.entities.PreferenciaUsuario;
import visorimagenes.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class UsuarioBOL {

    public Usuario guardar(Usuario usuario, String recon) {
        validar(usuario, recon);
        usuario.setContrasena(md5(usuario.getContrasena()));
        if (usuario.getId() > 0) {
            return new UsuarioDAO().actualizar(usuario);
        } else {
            return new UsuarioDAO().insertar(usuario);
        }
    }

    private void validar(Usuario usuario, String recon) {
        if (usuario == null) {
            throw new RuntimeException("Usuario Incorrecto");
        }

        if (usuario.getUsuario() == null || usuario.getUsuario().trim().isEmpty()) {
            throw new RuntimeException("Nombre de usuario requerido");
        }

        if (usuario.getCorreo() == null || usuario.getCorreo().trim().isEmpty()) {
            throw new RuntimeException("Correo requerido");
        }

        if (usuario.getContrasena() == null || usuario.getContrasena().trim().isEmpty()
                || usuario.getContrasena().length() < 8) {
            throw new RuntimeException("Contraseña con mínimo 8 caracteres");
        }
        if (recon == null || !recon.equals(usuario.getContrasena())) {
            throw new RuntimeException("Contraseñas no coinciden");
        }
    }

    //http://mirrors.ucr.ac.cr/apache//commons/codec/binaries/commons-codec-1.12-bin.zip
    private String md5(String pass) {
        String enc = DigestUtils.md5Hex(pass);
        return enc;
    }

    public boolean guardarPreferencia(PreferenciaUsuario pu, Usuario usuario) {
        if (pu == null || usuario == null) {
            throw new RuntimeException("Datos Inválidos");
        }

        if (pu.getId() <= 0) {
            throw new RuntimeException("Preferencia seleccionada inválida");
        }

        if (usuario.getId() <= 0) {
            throw new RuntimeException("Usuario seleccionado inválido");
        }

        if (pu.getValor() < 0) {
            throw new RuntimeException("Valor ingresado no válido");
        }

        return new UsuarioDAO().guardarPreferencia(pu, usuario.getId());
    }

    public Usuario autenticar(Usuario u) {
        validar(u, u.getContrasena());
        u.setContrasena(md5(u.getContrasena()));
        return new UsuarioDAO().autenticar(u);
    }

}

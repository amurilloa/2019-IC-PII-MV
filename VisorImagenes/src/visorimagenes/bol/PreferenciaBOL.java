/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes.bol;

import java.util.LinkedList;
import visorimagenes.dao.PreferenciaDAO;
import visorimagenes.entities.Preferencia;

/**
 *
 * @author allanmual
 */
public class PreferenciaBOL {

    public LinkedList<Preferencia> cargar() {
        return new PreferenciaDAO().cargar();
    }
    
}

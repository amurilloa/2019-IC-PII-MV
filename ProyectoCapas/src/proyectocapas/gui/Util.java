/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocapas.gui;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import proyectocapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class Util {

    public static void limpiar(Container container) {
        for (Component com : container.getComponents()) {
            if (com instanceof JTextField) {
                ((JTextField) com).setText("");
            } else if (com instanceof JComboBox) {
                ((JComboBox) com).setSelectedIndex(0);
            } else if (com instanceof JPanel) {
                limpiar((Container) com);
            }
        }
    }

    public static void asignarPermisos(Usuario logeado, JComponent component) {
        if (component instanceof JMenu) {
            for (Component menuComponent : ((JMenu) component).getMenuComponents()) {
                asignarPermisos(logeado, (JComponent) menuComponent);
            }
            if (component.getName() != null) {
                component.setVisible(logeado.permiso(component.getName()));
            }
        } else if (component instanceof JMenuItem) {
            if (component.getName() != null) {
                component.setVisible(logeado.permiso(component.getName()));
            }
        }
    }

}

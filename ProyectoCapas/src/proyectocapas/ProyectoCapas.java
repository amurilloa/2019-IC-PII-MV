/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocapas;

import proyectocapas.gui.FrmUsuario;

/**
 *
 * @author allanmual
 */
public class ProyectoCapas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //GUI->Interfaz Gráfica
        //BOL->Lógica de la aplicación: todas la validaciones y lógica de la app
        //DAO->Capa de datos: todos los métodos que leen o escriben datos(base datos, archivo).
        //Entities: Clases de los objetos(Persona, Cliente, Vehiculo, etc)
        /*
        
        WEB, DESKT, MOVIL
             JAVA, C#
   SQL SERVER, ORACLE, POSTGRESQL
        
        GUI-->BOL-->DAO
           ENTITIES
          
        */
        new FrmUsuario().setVisible(true);
    }
    
}

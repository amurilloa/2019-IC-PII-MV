/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocapas.entities;

import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Usuario {

    private String usuario;
    private String nombre;
    private String apellidoUno;
    private String apellidoDos;
    private String correo;
    private String contrasenna;
    private boolean tipo;
    private LinkedList<Acceso> accesos;

    public Usuario() {
        accesos = new LinkedList<>();
    }

    public Usuario(String usuario, String nombre, String apellidoUno, String apellidoDos, String correo, String contrasenna) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.correo = correo;
        this.contrasenna = contrasenna;
        accesos = new LinkedList<>();
    }

    public boolean permiso(String idPermiso) {
        for (Acceso acceso : accesos) {
            if (idPermiso.equals(acceso.getId())) {
                return true;
            }
        }
        return false;
    }

    public String getData() {
        String form = "%s,%s,%s,%s,%s,%s,%s";
        return String.format(form, usuario, nombre, apellidoUno, apellidoDos, correo, contrasenna, tipo);
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public boolean isTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

    public LinkedList<Acceso> getAccesos() {
        return accesos;
    }

    public void setAccesos(LinkedList<Acceso> accesos) {
        this.accesos = accesos;
    }

    @Override
    public String toString() {
        return usuario + " : " + nombre + " " + apellidoUno + " " + apellidoDos + " (" + correo + ")";
    }

}

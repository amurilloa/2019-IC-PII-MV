/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocapas.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author allanmual
 */
public class ManejoArchivos {

    public String leer(String ruta) throws IOException {
        String datos = "";
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            archivo = new File(ruta);

            if (!archivo.exists()) {
                archivo.getParentFile().mkdirs();
                archivo.createNewFile();
            }

            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            String linea;
            while ((linea = br.readLine()) != null) {
                datos += linea + "\n";
            }
        } catch (IOException e) {
            throw new IOException("Archivo no encontrado " + ruta);
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e2) {
                throw new IOException("No se pudo cerrar el archivo " + ruta);
            }
        }
        return datos;
    }

    public void escribir(String ruta, String datos) throws IOException {
        escribir(ruta, datos, false);
    }

    public void escribir(String ruta, String datos, Boolean pegar) throws IOException {
        FileWriter fichero = null;
        PrintWriter pw = null;

        try {
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.getParentFile().mkdirs();
                archivo.createNewFile();
            }
            
            fichero = new FileWriter(ruta, pegar);
            pw = new PrintWriter(fichero);
            pw.println(datos.replaceAll("\n", "\r\n"));
        } catch (IOException e) {
            throw new IOException("Archivo no encontrado " + ruta);
        } finally {
            try {
                if (fichero != null) {
                    fichero.close();
                }
            } catch (IOException e2) {
                throw new IOException("No se pudo cerrar el archivo " + ruta);
            }
        }

    }
}

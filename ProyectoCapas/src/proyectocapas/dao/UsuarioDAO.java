/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocapas.dao;

import java.util.LinkedList;
import proyectocapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class UsuarioDAO {

    private ManejoArchivos ma;
    private final String RUTA_USU = "datos/usuarios.txt";

    public UsuarioDAO() {
        ma = new ManejoArchivos();
    }

    public boolean insertar(Usuario u) {
        try {
            ma.escribir(RUTA_USU, u.getData(), true);
            return true;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de comunicación con el servidor");
        }
    }

    public void autenticar(Usuario u) {
        try {
            String[] lineas = ma.leer(RUTA_USU).split("\n");
            for (String linea : lineas) {
                String[] datos = linea.split(",");
                if (datos[5].equals(u.getContrasenna())) {
                    if (datos[0].equals(u.getUsuario()) || datos[4].equals(u.getCorreo())) {
                        cargarUsuario(u, datos);
                        return;
                    }
                }
            }
            u.setUsuario(null);
        } catch (Exception e) {
            throw new RuntimeException("No hay usuarios registrados");
        }
    }

    public LinkedList<Usuario> cargarUsuarios(String filtro) {
        
        LinkedList<Usuario> usuarios = new LinkedList<>();
        
        try {
            String[] lineas = ma.leer(RUTA_USU).split("\n");
            for (String linea : lineas) {
                if (linea.toLowerCase().contains(filtro.toLowerCase())) {
                    String[] datos = linea.split(",");
                    Usuario u = new Usuario();
                    cargarUsuario(u, datos);
                    usuarios.add(u);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("No hay usuarios registrados");
        }
        
        return usuarios;
    }

    private void cargarUsuario(Usuario u, String[] datos) {
        u.setUsuario(datos[0]);
        u.setNombre(datos[1]);
        u.setApellidoUno(datos[2]);
        u.setApellidoDos(datos[3]);
        u.setCorreo(datos[4]);
        u.setContrasenna("");
        u.setTipo(Boolean.parseBoolean(datos[6]));
        AccesoDAO adao = new AccesoDAO();
        u.setAccesos(adao.cargarAccesos(u.getUsuario()));
    }
}

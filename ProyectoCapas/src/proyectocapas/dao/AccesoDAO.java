/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocapas.dao;

import java.util.LinkedList;
import proyectocapas.entities.Acceso;
import proyectocapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class AccesoDAO {

    private ManejoArchivos ma;
    private final String RUTA_ACC = "datos/permisos.txt";
    private final String RUTA_USU_ACC = "datos/usuario_permisos.txt";

    public AccesoDAO() {
        ma = new ManejoArchivos();
    }

    public LinkedList<Acceso> cargarAccesos() {
        LinkedList<Acceso> accesos = new LinkedList<>();

        try {
            String[] lineas = ma.leer(RUTA_ACC).split("\n");
            for (String linea : lineas) {
                String[] datos = linea.split(",");
                accesos.add(cargarAcceso(datos));
            }
        } catch (Exception e) {
            throw new RuntimeException("No hay usuarios registrados");
        }

        return accesos;
    }

    private Acceso cargarAcceso(String[] datos) {
        Acceso a = new Acceso();
        a.setId(datos[0]);
        a.setDesc(datos[1]);
        return a;
    }

    public LinkedList<Acceso> cargarAccesos(String usuario) {
        LinkedList<Acceso> accesos = new LinkedList<>();
        try {
            String[] lineas = ma.leer(RUTA_USU_ACC).split("\n");
            for (String linea : lineas) {
                String[] datos = linea.split(",");
                if (datos[0].equals(usuario)) {
                    Acceso a = new Acceso();
                    a.setId(datos[1]);
                    accesos.add(a);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("No hay usuarios registrados");
        }
        return accesos;
    }

    public void registrarAcceso(Usuario usuario, Acceso sel) {
        try {
            String data = String.format("%s,%s", usuario.getUsuario(), sel.getId());
            ma.escribir(RUTA_USU_ACC, data, true);
        } catch (Exception e) {
            throw new RuntimeException("Problemas de comunicación con el servidor");
        }

    }
}

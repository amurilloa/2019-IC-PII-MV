/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocapas.bol;

import java.util.LinkedList;
import proyectocapas.dao.UsuarioDAO;
import proyectocapas.entities.Usuario;
import proyectocapas.gui.Main;

/**
 *
 * @author allanmual
 */
public class UsuarioBOL {

    private final int MIN_PASS = 8;

    public boolean registrar(Usuario u, String repass) {
        validar(u, repass);
        UsuarioDAO udao = new UsuarioDAO();
        return udao.insertar(u);
    }

    public void iniciarSesion(Usuario u) {
        if (u == null) {
            throw new RuntimeException("Usuario y/o contraseña inválidos");
        }

        if (u.getContrasenna().length() < MIN_PASS) {
            throw new RuntimeException("Usuario y/o contraseña inválidos");
        }

        if ((u.getUsuario() == null || u.getUsuario().isEmpty())
                && (u.getCorreo() == null || u.getCorreo().isEmpty())) {
            throw new RuntimeException("Usuario y/o contraseña inválidos");
        }

        new UsuarioDAO().autenticar(u);
    }

    private void validar(Usuario u, String repass) {
        if (u == null) {
            throw new RuntimeException("Datos de Usuario inválidos");
        }
        if (u.getContrasenna().length() < MIN_PASS) {
            throw new RuntimeException("La contraseña debe tener mínimo "
                    + MIN_PASS + " caracteres");
        }
        if (!u.getContrasenna().equals(repass)) {
            throw new RuntimeException("Las contraseñas no coinciden");
        }
        if (u.getNombre().isEmpty()) {
            throw new RuntimeException("Nombre requerido");
        }
        if (u.getApellidoUno().isEmpty()) {
            throw new RuntimeException("Primer Apellido requerido");
        }

        if (u.getUsuario().isEmpty()) {
            throw new RuntimeException("Usuario requerido");
        }
        if (u.getCorreo().isEmpty()) {
            throw new RuntimeException("Correo requerido");
        }
    }

    public LinkedList<Usuario> buscarUsuarios(String filtro) {
        if (filtro.length() >= 3) {
            return new UsuarioDAO().cargarUsuarios(filtro);
        } else {
            return new LinkedList<Usuario>();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocapas.bol;

import java.util.LinkedList;
import proyectocapas.dao.AccesoDAO;
import proyectocapas.entities.Acceso;
import proyectocapas.entities.Usuario;

/**
 *
 * @author allanmual
 */
public class AccesoBOL {

    public LinkedList<Acceso> cargarAccesos() {
        return new AccesoDAO().cargarAccesos();
    }

    public void registrarAcceso(Usuario usuario, Acceso sel) {
        if(usuario == null || sel == null){
            throw new RuntimeException("Favor seleccionar un usuario/acceso");
        }
        new AccesoDAO().registrarAcceso(usuario,sel);
    }
    
}

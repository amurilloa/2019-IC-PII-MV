/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author allanmual
 */
public class Diagnostico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Commit: Salvar de manera local los cambios
        //Pull: Traer los cambios de los compañeros de trabajo
        //Push: Subir los cambios al servidor (gitlab)
        
//      String frase = JOptionPane.showInputDialog("Digite una frase");
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite una frase: ");
        String frase = sc.nextLine();
        
        
        Logica log = new Logica(frase);
        
        
        System.out.println(log.mostrar(log.generar()));
       
    }
    
}

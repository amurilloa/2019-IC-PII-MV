/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

/**
 *
 * @author allanmual
 */
public class Logica {

    private char[] frase;

    public Logica(String frase) {
        this.frase = frase.toCharArray();

//        Alternativa
//        this.frase = new char[frase.length()];
//        for (int i = 0; i < frase.length(); i++) {
//            this.frase[i] = frase.charAt(i);
//        }
    }

    /**
     * Genera una matriz de 2 filas donde la primera fila contiene los 
     * carecteres de la frase ingresada y la segunda debe contener una X 
     * si la letra de la celda correspondiente de la primera fila es vocal o 
     * una O si es consonante
     * @return matriz de caracteres según la especificación del métodos
     */
    public char[][] generar() {
        char[] nuevo = new char[frase.length];

        for (int i = 0; i < frase.length; i++) {
            if ("aeiou".contains(String.valueOf(frase[i]).toLowerCase())) {
                nuevo[i] = 'x';
            } else if (Character.isAlphabetic(frase[i])) {
                nuevo[i] = 'o';
            } else {
                nuevo[i] = frase[i];
            }
        }

        return new char[][]{frase, nuevo};
    }

    public String mostrar(char[][] matriz) {
        String res = "";
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                res += matriz[i][j] + "|";
            }
            res += "\b\n";
        }
        return res;
    }

}

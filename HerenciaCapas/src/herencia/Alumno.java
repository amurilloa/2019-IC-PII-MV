/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author allanmual
 */
public class Alumno extends Persona {

    private String universidad;

    public Alumno(String universidad, String cedula, String nombre) {
        super(cedula, nombre);
        this.universidad = universidad;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public String quienSoy() {
        return getCedula() + " - " + getNombre() + " : " + universidad;
    }

    @Override
    public String saludar() {
        return "Soy " + getNombre() + " estoy en la " + universidad;
    }

}

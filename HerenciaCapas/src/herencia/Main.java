/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {

        Persona p = new Persona("", "");
        p.setCedula("206470762");
        p.setNombre("Allan Murillo");
        System.out.println(p.saludar());

            
        Alumno a = new Alumno("UTN","206520946", "Lineth Matamoros");
        System.out.println(a.quienSoy());
        System.out.println(a.saludar());
        
        
        Vehiculo v = new Vehiculo("HBX-692", "Toyota");

        Autobus at = new Autobus(70, "HBL-242", "Volvo");

        Camion c = new Camion(15, "CBL-231", "Freightliner");
        CamionCompartimientos cc = new CamionCompartimientos(5, 20, "CCC-235", "Freightliner");
        System.out.println(cc.capacidadCarga());
        System.out.println(cc.descripcion());

        Vehiculo v1 = new Vehiculo();
        Autobus at1 = new Autobus();
        Camion c1 = new Camion();
        CamionCompartimientos cc1 = new CamionCompartimientos();

        Object o = 1;
        o = "as";
        o = 'f';
        o = true;
        o = v1;
        o = at1;

        Vehiculo temp1 = new Camion(10, "GBH-121", "Toyota");
        Vehiculo temp2 = new CamionCompartimientos(5, 20, "CCC-235", "Freightliner");
        Vehiculo temp3 = new Autobus(70, "HBL-242", "Volvo");
        Vehiculo temp4 = new Vehiculo("HBS-123", "Toyota");
        System.out.println(c.getCarga());

        Vehiculo[] arreglo = {temp1, temp2, temp3, temp4};
        int[] num = {1, 2, 3, 4, 5, 6};

        for (Vehiculo vehiculo : arreglo) {
            System.out.println(vehiculo);
        }
        

//        for (Vehiculo temp : arreglo) {
//            System.out.println(temp);
//            if (temp instanceof CamionCompartimientos) {
//                System.out.println(((CamionCompartimientos) temp).capacidadCarga());
//                System.out.println(((CamionCompartimientos) temp).descripcion());
//            } else if (temp instanceof Camion) {
//                System.out.println(((Camion) temp).getCarga());
//            } else if (temp instanceof Autobus) {
//                System.out.println(((Autobus) temp).getAsientos());
//            } else {
//                System.out.println(temp.getMarca());
//                System.out.println(temp.getPlaca());
//            }
//        }
    }

}

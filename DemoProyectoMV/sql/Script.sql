/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  allanmual
 * Created: 27-mar-2019
 */

CREATE TABLE servicios(
	id serial, 
	codigo text NOT NULL,
	servicio text NOT NULL,
	descripcion text, 
	precio numeric DEFAULT 0, 
	repetitivo boolean DEFAULT false, 
	activo boolean DEFAULT true,
	CONSTRAINT pk_servicios PRIMARY KEY(id),
	CONSTRAINT unq_ser_cod UNIQUE(codigo)
);

INSERT INTO servicios(codigo, servicio, descripcion, precio, repetitivo)
	VALUES ('CA-01', 'Cambio de Aceite #1', 'Cambio de aceite y filtro correspondiente', 10000, true);
INSERT INTO servicios(codigo, servicio, descripcion, precio, repetitivo)
	VALUES ('CA-02', 'Cambio de Aceite #2', 'Cambio de aceite, filtro correspondiente y filtro de aire', 15000, true);
SELECT * FROM servicios;

DROP TABLE insumos;

CREATE TABLE insumos(
	id serial, 
	codigo text NOT NULL,
	insumo text NOT NULL,
	descripcion text, 
	precio numeric DEFAULT 0,
	cantidad int DEFAULT 0,
	activo boolean DEFAULT true,
	CONSTRAINT pk_insumos PRIMARY KEY(id),
	CONSTRAINT unq_ins_cod UNIQUE(codigo)
);
-- ALTER TABLE insumos ADD COLUMN cantidad int DEFAULT 0;

INSERT INTO insumos(codigo, insumo, descripcion, precio, cantidad)
	VALUES ('AC-01', 'Aceite', 'Aceite genérico', 5500, 20),
	 ('FC-01', 'Filtro de Aceite', 'Filtro de aceite genérico', 2500, 100),
	 ('FA-01', 'Filtro de Aire', 'Filtro de aire genérico', 10000, 10);
	 
	 
	 
select * from insumos;

CREATE TABLE ser_ins(
	id serial,
	id_servicios int, 
	id_insumos int, 
	cantidad int, 
	CONSTRAINT pk_serins PRIMARY KEY(id),
	CONSTRAINT unq_serins_serins UNIQUE(id_servicios, id_insumos),
	CONSTRAINT fk_serins_ser FOREIGN KEY(id_servicios) REFERENCES servicios(id),
	CONSTRAINT fk_serins_ins FOREIGN KEY(id_insumos) REFERENCES insumos(id)
);
drop table ser_ins
select * from ser_ins

INSERT INTO ser_ins(id_servicios, id_insumos, cantidad) VALUES(1, 1, 8);
INSERT INTO ser_ins(id_servicios, id_insumos, cantidad) VALUES(1, 2, 1);

INSERT INTO ser_ins(id_servicios, id_insumos, cantidad) VALUES(2, 1, 8);
INSERT INTO ser_ins(id_servicios, id_insumos, cantidad) VALUES(2, 2, 1);
INSERT INTO ser_ins(id_servicios, id_insumos, cantidad) VALUES(2, 3, 1);
SELECT * FROM servicios;
-- 1	"CA-01"	"Cambio de Aceite #1"	"Cambio de aceite y filtro correspondiente"	"10000"	true	true
-- 2	"CA-02"	"Cambio de Aceite #2"	"Cambio de aceite, filtro correspondiente y filtro de aire"	"15000"	true	true
SELECT * FROM insumos;
-- 1	"AC-01"	"Aceite"	"Aceite genérico"	"5500"	true
-- 2	"FC-01"	"Filtro de Aceite"	"Filtro de aceite genérico"	"2500"	true
-- 3	"FA-01"	"Filtro de Aire"	"Filtro de aire genérico"	"10000"	true
SELECT * FROM ser_ins;
-- 1	1	1	8
-- 2	1	2	1
-- 3	2	1	8
-- 4	2	2	1
-- 5	2	3	1
select s.codigo,s.servicio, (s.precio +sum(i.precio* si.cantidad)) total
from servicios s 
join ser_ins si on(s.id = si.id_servicios) 
join insumos i on(si.id_insumos = i.id)
group by s.codigo,s.servicio, s.precio



-- ID|ID_INSUMO|ID_MARCA|ID_MODELO|ANNO
-- 1 .   1 .      1 .      null .   null
-- 2 .   2 .      1 .       1 .     null
-- 3 .   3 .      1 .       1      2002   mínimo 
-- 4 .   4 .     null .     3				

-- Toyota .  [X]
--  Hilux .  [X]
--  2003     






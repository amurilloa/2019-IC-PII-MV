/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectomv.dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author allanmual
 */
public class Conexion {

    private static final String DRIVER = "org.postgresql.Driver";
    private static final String TIPO = "jdbc:postgresql://";
    private static final String SERVER = "localhost";
    private static final String PUERTO = "5432";
    private static final String DB = "taller_mecanico_mv";
    private static final String USER = "postgres";
    private static final String PASS = "postgres";

    public static Connection conexion() throws Exception {
        Connection c = null;
        try {
            Class.forName(DRIVER);
            c = DriverManager.getConnection(TIPO + SERVER + ":" + PUERTO + "/"
                    + DB, USER, PASS);
            return c;
        } catch (Exception ex) {
            throw new Exception("0001:Problemas con la conexión al servidor");
        }
    }
    
//    public static void main(String[] args) {
//        try {
//            System.out.println("Conectando");
//            conexion();
//            System.out.println("Listo");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}

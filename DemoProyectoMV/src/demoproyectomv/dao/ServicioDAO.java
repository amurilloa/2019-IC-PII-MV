/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectomv.dao;

import demoproyectomv.entities.MiError;
import demoproyectomv.entities.Servicio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import org.postgresql.util.PSQLException;

/**
 *
 * @author allanmual
 */
public class ServicioDAO {

    public LinkedList<Servicio> cargar(String filtro, boolean todos, boolean inactivos) {

        LinkedList<Servicio> servicios = new LinkedList<>();

        try (Connection con = Conexion.conexion()) {
            String sql = "select id, codigo, servicio, descripcion, precio,"
                    + " repetitivo, activo from servicios ";

            sql += !todos ? " where lower(codigo) like lower(?) or lower(servicio) like lower(?) " : "";
            sql += !todos && !inactivos ? " and activo = true" : "";
            sql += todos && !inactivos ? " where activo = true" : "";

            PreparedStatement ps = con.prepareStatement(sql);
            if (!todos) {
                ps.setString(1, "%" + filtro + "%");
                ps.setString(2, "%" + filtro + "%");
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                servicios.add(cargarServicio(rs));
            }

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor, al recuperar los servicios");
        }

        return servicios;
    }

    private Servicio cargarServicio(ResultSet rs) throws SQLException {
        Servicio temp = new Servicio();
        temp.setId(rs.getInt("id"));
        temp.setCodigo(rs.getString("codigo"));
        temp.setServicio(rs.getString("servicio"));
        temp.setDescripcion(rs.getString("descripcion"));
        temp.setPrecio(rs.getDouble("precio"));
        temp.setRepetitivo(rs.getBoolean("repetitivo"));
        temp.setActivo(rs.getBoolean("activo"));
        return temp;
    }

    public void eliminar(Servicio servicio) {
        try (Connection con = Conexion.conexion()) {
            String sql = "update servicios set activo = false where id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, servicio.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor, al eliminar el servicio");
        }
    }

    public void actualizar(Servicio servicio) {
        try (Connection con = Conexion.conexion()) {
            String sql = "update servicios set codigo=?, servicio=?, descripcion=?,"
                    + " precio=?, repetitivo=?, activo=? where id = ?";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, servicio.getCodigo());
            ps.setString(2, servicio.getServicio());
            ps.setString(3, servicio.getDescripcion());
            ps.setDouble(4, servicio.getPrecio());
            ps.setBoolean(5, servicio.isRepetitivo());
            ps.setBoolean(6, servicio.isActivo());
            ps.setInt(7, servicio.getId());
            ps.executeUpdate();

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor, al actualizar el servicio");
        }
    }

    public void insertar(Servicio servicio) {
        try (Connection con = Conexion.conexion()) {
            String sql = "INSERT INTO servicios("
                    + "	codigo, servicio, descripcion, precio, repetitivo, activo)"
                    + "	VALUES ( ?, ?, ?, ?, ?, ?)";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, servicio.getCodigo());
            ps.setString(2, servicio.getServicio());
            ps.setString(3, servicio.getDescripcion());
            ps.setDouble(4, servicio.getPrecio());
            ps.setBoolean(5, servicio.isRepetitivo());
            ps.setBoolean(6, servicio.isActivo());
            ps.executeUpdate();

        } catch (Exception e) {
            throw new MiError(e.getMessage());
        }
    }

}

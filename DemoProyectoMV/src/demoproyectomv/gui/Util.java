/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectomv.gui;

import demoproyectomv.entities.MiError;
import java.awt.Color;
import javax.swing.JLabel;

/**
 *
 * @author allanmual
 */
public class Util {
    
    public static final int DEFAULT = 0;
    public static final int EXITO = 1;
    public static final int ALERT = 2;
    public static final int ERROR = 3;
    
    public static void mostrar(JLabel lbl, String msj, int TIPO) {
        Color c = TIPO == 0
                ? Color.BLACK : TIPO == 1
                        ? Color.GREEN : TIPO == 2
                                ? Color.YELLOW : Color.RED;
        lbl.setForeground(c);
        lbl.setText(msj);
    }
    
    public static void mostrar(JLabel lbl, String msj) {
        lbl.setText(msj);
    }
    
    public static void mostrar(JLabel lbl, Exception e) {
        lbl.setForeground(Color.red);
        if (e instanceof MiError) {
            MiError e1 = (MiError) e;
            lbl.setText(e1.getMensaje());
        } else {
            lbl.setText("Intente nuevamente");
        }
    }
    
    public static void limpiar(JLabel lbl) {
        lbl.setText(" ");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectomv;

import demoproyectomv.gui.FrmServicio;
import static java.awt.Frame.MAXIMIZED_BOTH;

/**
 *
 * @author allanmual
 */
public class DemoProyectoMV {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FrmServicio frm = new FrmServicio();
        frm.setExtendedState(MAXIMIZED_BOTH);
        frm.pack();
        frm.setVisible(true);
    }

}

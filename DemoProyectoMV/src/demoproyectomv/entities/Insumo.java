/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectomv.entities;

/**
 *
 * @author allanmual
 */
public class Insumo {

    protected int id;
    protected String codigo;
    protected String insumo;
    protected double precio;
    protected int cantidad;
    protected boolean activo;
//    protected int marca; 
//    protected int modelo; 
//    protected int anno; 
    
    
    
    public Insumo() {
    }

    public Insumo(int id, String codigo, String insumo, double precio, int cantidad, boolean activo) {
        this.id = id;
        this.codigo = codigo;
        this.insumo = insumo;
        this.precio = precio;
        this.cantidad = cantidad;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getInsumo() {
        return insumo;
    }

    public void setInsumo(String insumo) {
        this.insumo = insumo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return codigo + "-" + insumo + "(" + (activo ? "\u2713" : "X") + (cantidad>0 ? "\u2713" : "X") + ")";
    }

}

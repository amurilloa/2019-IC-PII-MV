/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectomv.entities;

/**
 *
 * @author allanmual
 */
public class MiError extends RuntimeException {

    private String mensaje;

    public MiError(String message) {
        super(message);
        this.mensaje = message;
    }

    public String getMensaje() {
        return procesarMensaje();
    }

    private String procesarMensaje() {
        if (mensaje.contains("unq_")) {
            if (mensaje.contains("_cod")) {
                return "Código ya está siendo utilizado";
            } else if (mensaje.contains("_ced")) {
                return "Cédula ya está siendo utilizado";
            } else {
                return "Dato ya se encuentra registrado";
            }
        } else if (mensaje.contains("0001")) {
            return "Problemas de conexión con el servidor";
        }
        return mensaje;
    }

}

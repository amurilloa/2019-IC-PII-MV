/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectomv.entities;

/**
 *
 * @author allanmual
 */
public class Servicio {

    private int id;
    private String servicio;
    private String codigo;
    private String descripcion;
    private double precio;
    private boolean repetitivo;
    private boolean activo;

    public Servicio() {
        activo = true;
    }

    public Servicio(int id, String servicio, String codigo, String descripcion, double precio, boolean repetitivo, boolean activo) {
        this.id = id;
        this.servicio = servicio;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.repetitivo = repetitivo;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isRepetitivo() {
        return repetitivo;
    }

    public void setRepetitivo(boolean repetitivo) {
        this.repetitivo = repetitivo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return codigo + " : " + servicio;
    }

}

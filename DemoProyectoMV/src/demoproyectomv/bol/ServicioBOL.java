/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectomv.bol;

import demoproyectomv.dao.ServicioDAO;
import demoproyectomv.entities.MiError;
import demoproyectomv.entities.Servicio;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class ServicioBOL {

    public LinkedList<Servicio> buscar(String filtro, boolean todos, boolean inactivos) {
        if (!todos && filtro.trim().length() < 3) {
            throw new MiError("Filtrar con un mínimo de 3 caracteres");
        }
        return new ServicioDAO().cargar(filtro, todos, inactivos);
    }

    public void eliminar(Servicio servicio) {
        if (servicio == null || servicio.getId() <= 0) {
            throw new MiError("Favor seleccionar un servicio");
        }
        new ServicioDAO().eliminar(servicio);
    }

    public void guardar(Servicio servicio) {
        validar(servicio);
        if (servicio.getId() > 0) {
            new ServicioDAO().actualizar(servicio);
        } else {
            new ServicioDAO().insertar(servicio);
        }
    }

    private void validar(Servicio servicio) {
        if (servicio == null) {
            throw new MiError("Favor seleccionar el servicio");
        }

        if (servicio.getCodigo() == null || servicio.getCodigo().trim().isEmpty()) {
            throw new MiError("Código requerido");

        }

        if (servicio.getServicio() == null || servicio.getServicio().trim().isEmpty()) {
            throw new MiError("Código requerido");

        }

        if (servicio.getPrecio() < 0) {
            throw new MiError("Precio no puede ser negativo");
        }
    }

}

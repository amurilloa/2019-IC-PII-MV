/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectomv.bol;

import demoproyectomv.dao.InsumoDAO;
import demoproyectomv.entities.Insumo;
import demoproyectomv.entities.InsumoServicio;
import demoproyectomv.entities.Servicio;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class InsumoBOL {

    public LinkedList<InsumoServicio> cargarInsumosReq(String filtro, boolean todos, Servicio servicio) {
        validar(filtro, todos, servicio);
        return new InsumoDAO().cargarReq(filtro, todos, servicio.getId());
    }

    public LinkedList<Insumo> cargarInsumosDis(String filtro, boolean todos, Servicio servicio) {
        validar(filtro, todos, servicio);
        return new InsumoDAO().cargarDis(filtro, todos, servicio.getId());
    }

    private void validar(String filtro, boolean todos, Servicio servicio) {
        if (servicio == null || servicio.getId() <= 0) {
            throw new RuntimeException("Debe seleccionar un servicio");
        }

        if (!todos && filtro.trim().length() <= 2) {
            throw new RuntimeException("Mínimo 2 caracteres para filtrar");
        }
    }

}

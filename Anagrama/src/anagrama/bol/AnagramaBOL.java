/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anagrama.bol;

import anagrama.dao.AnagramaDAO;
import anagrama.entities.Palabra;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author allanmual
 */
public class AnagramaBOL {

    private Palabra palabra;
    private AnagramaDAO ado;

    public AnagramaBOL() {
        ado = new AnagramaDAO();
    }

    private void cargarPalabra() {
        palabra = ado.cargarPalabra();
    }

    /**
     * Obtiene una palabra y la retorna de manera desordenada
     *
     * @return String palabra desordenada
     */
    public String getPalabraDes() {
        cargarPalabra();
        String[] letras = palabra.getPalabra().split("");
        List<String> lista = Arrays.asList(letras);
        Collections.shuffle(lista);
        StringBuilder sb = new StringBuilder();
        for (String letra : lista) {
            sb.append(letra);
        }
        return sb.toString();
    }

    public boolean revisar(String palabra) {

        palabra = palabra.toUpperCase();

        if (this.palabra.getPalabra().equalsIgnoreCase(palabra)) {
            return true;
        }

        if (this.palabra.getPalabra().length() != palabra.length()) {
            return false;
        }

        for (String letra : this.palabra.getPalabra().split("")) {
            if (!palabra.contains(letra)) {
                return false;
            }       
        }
        return ado.revisar(palabra);

    }

}

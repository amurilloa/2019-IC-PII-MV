/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anagrama.dao;

import anagrama.entities.Palabra;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author allanmual
 */
public class AnagramaDAO {

    public Palabra cargarPalabra() {
        try (Connection con = Conexion.conexion()) {
            String sql = "select id, palabra, activo "
                    + " from anagrama.palabras "
                    + " order by random() limit 1";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return cargarPalabra(rs);
            } else {
                return null;
            }

        } catch (Exception ex) {
            throw new RuntimeException("Problemas al cargar la palabra");
        }
    }

    private Palabra cargarPalabra(ResultSet rs) throws SQLException {
        Palabra p = new Palabra();
        p.setId(rs.getInt("id"));
        p.setPalabra(rs.getString("palabra"));
        p.setActivo(rs.getBoolean("activo"));
        return p;
    }

    public boolean revisar(String palabra) {
        try (Connection con = Conexion.conexion()) {
            String sql = "SELECT id FROM anagrama.palabras"
                    + " WHERE UPPER(palabra) = UPPER(?)";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, palabra);

            ResultSet rs = ps.executeQuery();

            return rs.next();

        } catch (Exception e) {
            e.printStackTrace();
            //throw new RuntimeException("Palabra no encontrada!!");
        }
        return false;
    }

}
